import lottoNumerot from './lotteryTicketsGenerator';
import TweetGenerator from './tweetGenerator';
import Bot from "./bot";
import fs from "fs";
import { lotteryTicketsObject } from "./Interfaces";
// Declarations here.
const bot = new Bot();
const Lotto = new lottoNumerot();
let tweetThisMessage: string;
let winningTicketInfo = {
    index: null,
    matches: 0
};
let flattenedLotteryNumbers = [];
let awardsObject = Lotto.awardsObject;
let lotteryTickets: Array<object>;
const currentWeek = Lotto.getWeek(new Date());
let statsInfo = new Array<lotteryTicketsObject>(); //JSON.parse(data.toString());
const tweetMessages = new TweetGenerator(currentWeek);
const jsonFileAddress = "./lottoStats.json";

/**
 * I have to decide what to do when I'm running the script.
 * Because the winning numbers are generated waay later
 * Like, the tickets need to be sent before 21:45
 * And the results are available usually 22:05 * 
 */
bot.getTweets(1)
    // .getLastMessage()
    
    .then((lastMessage: Array<string>) => {        
        if (lastMessage[0] === "fuuuck")
            process.exit(1);
        tweetThisMessage = tweetMessages.getTweetText("begin");
        return bot.tweetMessage(tweetThisMessage);
    })
    .then(() => {
        lotteryTickets = Lotto.ticketList;
        fs.readFile(jsonFileAddress, (err, data) => {
            statsInfo.push({ "week": currentWeek.toString(), tickets: lotteryTickets });
        });
        // Before we tweet about the numbers, we have to store them
        fs.writeFile(jsonFileAddress, JSON.stringify(statsInfo), (error?: NodeJS.ErrnoException) => {
            if (typeof error === "undefined" || error === null) {
                console.info("Tickets have been generated.");
                lotteryTickets.forEach((numberArray: Array<number>) => {
                    numberArray.forEach((number) => {
                        if (flattenedLotteryNumbers.includes(number) == false)
                            flattenedLotteryNumbers.push(number);
                    });
                });
                // 2nd tweet
                tweetThisMessage = tweetMessages.getTweetText("numbersGenerated", flattenedLotteryNumbers.toString());
                return bot.tweetMessage(tweetThisMessage);
            } else {
                console.error("Storing generated numbers failed.");
                console.error("error: ", error);
                throw error;
            }
        });
    }).then(() => {
        // TODO: Change this thing so that it can be run in single row of then-functions.
        // Get the winning Numbers
        Lotto.getWinningNumbers().then((results: Array<string>) => {
            // Then let's go through my tickets and see if we won anything.
            lotteryTickets.forEach((numberArray: Array<number>) => {
                let matches: number = 0;
                numberArray.forEach((number: number, index: number) => {
                    // Since the numbers don't have to be in the same order as in the winningNumbers array [citation needed]
                    // We'll just check if it's in there..
                    if (results.includes(number.toString()))
                        matches++;
                    if (index === numberArray.length - 1) {
                        // TODO: Maybe move this to some other place?
                        // Or consider using return. Maybe it's needed, maybe not, idk
                        if (matches > winningTicketInfo.matches) {
                            winningTicketInfo.index = index;
                            winningTicketInfo.matches = matches;
                        }
                    }
                });
            });
        }).then(() => {
            // Let's see what we won.
            if (winningTicketInfo.matches > 3) {
                // Because the object is changed, we have to change this also.
                // What fun.
                let award: number;
                if (winningTicketInfo.matches === 4) {
                    award = awardsObject.four;
                }
                else if (winningTicketInfo.matches === 5) {
                    award = awardsObject.five;
                }
                else if (winningTicketInfo.matches === 6) {
                    award = awardsObject.six;
                }
                else if (winningTicketInfo.matches === 7) {
                    award = awardsObject.seven;
                }
                // 3rd tweet
                const awardPriceAmount = tweetMessages.awardToString(award);
                return tweetMessages.getTweetText("resultsReceived", awardPriceAmount, winningTicketInfo.matches);
            } else {
                return tweetMessages.getTweetText("resultsReceived", "", winningTicketInfo.matches);
            }
        }).then((twitterPost) => {
            // Just in case I'll place the tweeting to here, async functionalities can be tricky.
            return bot.tweetMessage(twitterPost);
        }).then(() => {
            // And then we should probably close down.
            process.exit(0);
        }).catch((error: any) => {
            console.error("Getting winning numbers failed");
            console.error("Failure reason: ", error.toString());
            process.exit(1);
        });
    })
    .catch((error: Error) => {
    console.error("Main procedure failure");
    console.error("Error: ", error);
    process.exit(1);
});