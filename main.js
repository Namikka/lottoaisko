"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lotteryTicketsGenerator_1 = __importDefault(require("./lotteryTicketsGenerator"));
const tweetGenerator_1 = __importDefault(require("./tweetGenerator"));
const bot_1 = __importDefault(require("./bot"));
const fs_1 = __importDefault(require("fs"));
// Declarations here.
const bot = new bot_1.default();
const Lotto = new lotteryTicketsGenerator_1.default();
let tweetThisMessage;
let winningTicketInfo = {
    index: null,
    matches: 0
};
let flattenedLotteryNumbers = [];
let awardsObject = Lotto.awardsObject;
let lotteryTickets;
const currentWeek = Lotto.getWeek(new Date());
let statsInfo = new Array(); //JSON.parse(data.toString());
const tweetMessages = new tweetGenerator_1.default(currentWeek);
const jsonFileAddress = "./lottoStats.json";
/**
 * I have to decide what to do when I'm running the script.
 * Because the winning numbers are generated waay later
 * Like, the tickets need to be sent before 21:45
 * And the results are available usually 22:05 *
 */
bot.getTweets(1)
    // .getLastMessage()
    .then((lastMessage) => {
    if (lastMessage[0] === "fuuuck")
        process.exit(1);
    tweetThisMessage = tweetMessages.getTweetText("begin");
    return bot.tweetMessage(tweetThisMessage);
})
    .then(() => {
    lotteryTickets = Lotto.ticketList;
    fs_1.default.readFile(jsonFileAddress, (err, data) => {
        statsInfo.push({ "week": currentWeek.toString(), tickets: lotteryTickets });
    });
    // Before we tweet about the numbers, we have to store them
    fs_1.default.writeFile(jsonFileAddress, JSON.stringify(statsInfo), (error) => {
        if (typeof error === "undefined" || error === null) {
            console.info("Tickets have been generated.");
            lotteryTickets.forEach((numberArray) => {
                numberArray.forEach((number) => {
                    if (flattenedLotteryNumbers.includes(number) == false)
                        flattenedLotteryNumbers.push(number);
                });
            });
            // 2nd tweet
            tweetThisMessage = tweetMessages.getTweetText("numbersGenerated", flattenedLotteryNumbers.toString());
            return bot.tweetMessage(tweetThisMessage);
        }
        else {
            console.error("Storing generated numbers failed.");
            console.error("error: ", error);
            throw error;
        }
    });
}).then(() => {
    // TODO: Change this thing so that it can be run in single row of then-functions.
    // Get the winning Numbers
    Lotto.getWinningNumbers().then((results) => {
        // Then let's go through my tickets and see if we won anything.
        lotteryTickets.forEach((numberArray) => {
            let matches = 0;
            numberArray.forEach((number, index) => {
                // Since the numbers don't have to be in the same order as in the winningNumbers array [citation needed]
                // We'll just check if it's in there..
                if (results.includes(number.toString()))
                    matches++;
                if (index === numberArray.length - 1) {
                    // TODO: Maybe move this to some other place?
                    // Or consider using return. Maybe it's needed, maybe not, idk
                    if (matches > winningTicketInfo.matches) {
                        winningTicketInfo.index = index;
                        winningTicketInfo.matches = matches;
                    }
                }
            });
        });
    }).then(() => {
        // Let's see what we won.
        if (winningTicketInfo.matches > 3) {
            // Because the object is changed, we have to change this also.
            // What fun.
            let award;
            if (winningTicketInfo.matches === 4) {
                award = awardsObject.four;
            }
            else if (winningTicketInfo.matches === 5) {
                award = awardsObject.five;
            }
            else if (winningTicketInfo.matches === 6) {
                award = awardsObject.six;
            }
            else if (winningTicketInfo.matches === 7) {
                award = awardsObject.seven;
            }
            // 3rd tweet
            const awardPriceAmount = tweetMessages.awardToString(award);
            return tweetMessages.getTweetText("resultsReceived", awardPriceAmount, winningTicketInfo.matches);
        }
        else {
            return tweetMessages.getTweetText("resultsReceived", "", winningTicketInfo.matches);
        }
    }).then((twitterPost) => {
        // Just in case I'll place the tweeting to here, async functionalities can be tricky.
        return bot.tweetMessage(twitterPost);
    }).then(() => {
        // And then we should probably close down.
        process.exit(0);
    }).catch((error) => {
        console.error("Getting winning numbers failed");
        console.error("Failure reason: ", error.toString());
        process.exit(1);
    });
})
    .catch((error) => {
    console.error("Main procedure failure");
    console.error("Error: ", error);
    process.exit(1);
});
//# sourceMappingURL=main.js.map