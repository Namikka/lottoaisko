"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const keyList = __importStar(require("./twitterConfig"));
const twitter_api_v2_1 = __importDefault(require("twitter-api-v2"));
const initClient = new twitter_api_v2_1.default({
    appKey: keyList.default.appKey,
    appSecret: keyList.default.appSecret,
    accessToken: keyList.default.accessToken,
    accessSecret: keyList.default.accessSecret,
}).readWrite;
const development = true;
const user_id = "1215706636158935041";
let getParams = {
    exclude: 'replies'
};
class Bot {
    constructor() {
        this.message = "";
        this.loggedinClient = initClient.v2;
    }
    tweetMessage(message) {
        if (message.length > 0) {
            if (development) {
                console.log(`DEVELOPMENT MODE Nyt pitäisi twiitata viesti: ${message}`);
                return Promise.resolve(true);
            }
            else {
                console.log(`TESTING/PRODUCTION MODE Nyt pitäisi twiitata viesti: ${message}`);
                return this.loggedinClient.tweet(message)
                    .then((tweet) => {
                    return Promise.resolve(true);
                }).catch(function (error) {
                    console.error(`FAILED at tweeting message ${message}`);
                    error.data.errors.forEach((err) => {
                        console.error(`Code: ${err.type} - ${err.title}`);
                    });
                    return Promise.reject(false);
                });
            }
        }
        else {
            return Promise.reject(false);
        }
    }
    async getLastMessage() {
        return this.loggedinClient.userTimeline(user_id, getParams)
            .then((tweets) => {
            const latestTweet = tweets.data[0].full_text;
            return Promise.resolve(latestTweet);
        }).catch((error) => {
            console.error("getLastMessage error happened:");
            console.error("Received error:", error);
            return Promise.reject("fuuuck");
        });
    }
    async getTweets(count = 1) {
        getParams.max_results = count;
        return this.loggedinClient.userTimeline(user_id, getParams)
            .then((tweets) => {
            let returnableTweets = new Array();
            tweets.data.data.forEach((t) => returnableTweets.push(t.text));
            if (getParams.max_results > 1) {
                console.log("More than 1 tweet wanted");
                console.log("tweets.tweets: ", tweets.tweets);
                console.log("tweets.data: ", tweets.data);
            }
            return Promise.resolve(returnableTweets);
        }).catch((error) => {
            console.error("getLastMessage error happened:");
            console.error("Received error:", error);
            return Promise.reject(new Array("fuuuck"));
        });
    }
}
exports.default = Bot;
//# sourceMappingURL=bot.js.map