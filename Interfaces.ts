interface lotteryTicketsObject {
    week: string;
    tickets: Array<object>;
}
interface tweetStatus {
    status: string;
    additionalString?: string|null;
    results?: number
}
export {lotteryTicketsObject, tweetStatus}