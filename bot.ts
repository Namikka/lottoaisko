import * as keyList from './twitterConfig';
import TwitterApi, { TweetV2UserTimelineParams, TwitterApiError, TwitterApiReadWrite, TweetUserTimelineV2Paginator, ErrorV2, TweetV2PostTweetResult } from 'twitter-api-v2';
import TwitterApiv2ReadWrite from 'twitter-api-v2/dist/v2/client.v2.write';


const initClient = new TwitterApi({
  appKey: keyList.default.appKey,
  appSecret: keyList.default.appSecret,
  accessToken: keyList.default.accessToken,
  accessSecret: keyList.default.accessSecret,
}).readWrite;

const development = true;
const user_id = "1215706636158935041";
let getParams: TweetV2UserTimelineParams = {
  exclude: 'replies'
};

export default class Bot {
  private message: string;
  private loggedinClient: TwitterApiv2ReadWrite

  constructor() {
    this.message = "";
    this.loggedinClient = initClient.v2;
  }

  public tweetMessage(message: string): Promise<boolean> {
    if (message.length > 0) {
      if (development) {
        console.log(`DEVELOPMENT MODE Nyt pitäisi twiitata viesti: ${message}`);
        return Promise.resolve(true);
      } else {
        console.log(`TESTING/PRODUCTION MODE Nyt pitäisi twiitata viesti: ${message}`);
        return this.loggedinClient.tweet(message)
          .then((tweet: TweetV2PostTweetResult) => {
            return Promise.resolve(true);
          }).catch(function (error: TwitterApiError) {
            console.error(`FAILED at tweeting message ${message}`);
            error.data.errors.forEach((err: ErrorV2) => {
              console.error(`Code: ${err.type} - ${err.title}`);
            });
            return Promise.reject(false);
          });
      }
    } else {
      return Promise.reject(false);
    }
  }

  public async getLastMessage(): Promise<string> {
    return this.loggedinClient.userTimeline(user_id, getParams)
      .then((tweets: TweetUserTimelineV2Paginator) => {
        const latestTweet = tweets.data[0].full_text;
        return Promise.resolve(latestTweet);
      }).catch((error: TwitterApiError) => {
        console.error("getLastMessage error happened:");
        console.error("Received error:", error);
        return Promise.reject("fuuuck");
      });
  }

  public async getTweets(count = 1): Promise<Array<string>> {
    getParams.max_results = count;
    return this.loggedinClient.userTimeline(user_id, getParams)
      .then((tweets: TweetUserTimelineV2Paginator) => {
        let returnableTweets = new Array();
        tweets.data.data.forEach((t) => returnableTweets.push(t.text));
        if (getParams.max_results > 1) {
          console.log("More than 1 tweet wanted");
          console.log("tweets.tweets: ", tweets.tweets);
          console.log("tweets.data: ", tweets.data);
        }
        return Promise.resolve(returnableTweets)
      }).catch((error: TwitterApiError) => {
        console.error("getLastMessage error happened:");
        console.error("Received error:", error);
        return Promise.reject(new Array("fuuuck"));
      });
  }
}
