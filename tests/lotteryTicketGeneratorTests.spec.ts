import assert from "assert";
import { expect } from "chai";
import lottoNumerot from "../lotteryTicketsGenerator";

describe("Lottery ticket generator", () => {
    let lotteryNumberGenerator: lottoNumerot;
    beforeEach(() => {
        lotteryNumberGenerator = new lottoNumerot();
    });
    it("should generate a lottery number generator", () => {
        // Arrange && Act
        const newLotteryNumberGenerated = new lottoNumerot()
        // Assert
        expect(newLotteryNumberGenerated).not.to.be.undefined;
        expect(newLotteryNumberGenerated).to.have.property("ticketList");
        expect(newLotteryNumberGenerated.ticketList).to.be.a("Array", "Ticketlist was not an array.");
        expect(newLotteryNumberGenerated.ticketList.length).to.equal(newLotteryNumberGenerated.tickets, `Ticketlist length was ${newLotteryNumberGenerated.lotteryRowMaxLength} instead of ${newLotteryNumberGenerated.tickets} `);
        expect(newLotteryNumberGenerated).to.have.property("awardsObject");
        expect(newLotteryNumberGenerated).to.have.property("tickets");
        expect(newLotteryNumberGenerated).to.have.property("lotteryRowMaxLength");
        expect(newLotteryNumberGenerated).to.have.property("maxLotteryNumber");
        expect(newLotteryNumberGenerated).to.have.property("minLotteryNumber");
        expect(newLotteryNumberGenerated).to.have.property("veikkausAPIAddress");
    });

    it("should generate right number of lottery tickets", () => {
        // Arrange &  Act
        const lotteryTicketListLength = lotteryNumberGenerator.ticketList.length
        
        // Assert
        expect(lotteryTicketListLength).to.equal(lotteryNumberGenerator.tickets);
    });

    it("should generate right number of numbers to each lottery ticket", () => {
        // Arrange 
        const lotteryTicketNumberLength = lotteryNumberGenerator.lotteryRowMaxLength

        // Act
        lotteryNumberGenerator.ticketList.forEach((ticketListRow:Array<number>) => {
            // Assert
            expect(ticketListRow.length).to.equal(lotteryTicketNumberLength);
        });
    });

    it("should generate numbers to each lottery ticket", () => {
        // Arrange & Act
        lotteryNumberGenerator.ticketList.forEach((ticketListRow:Array<number>) => {
            // Assert
            ticketListRow.forEach((lotteryNumber:number) => {
                expect(lotteryNumber).to.be.a("number", `${lotteryNumber.toString()} was not a number`);
                
            })
        });
    });
    
    it("should generate random unique numbers to each lottery ticket", () => {
        // Arrange & Act 
        lotteryNumberGenerator.ticketList.forEach((ticketListRow:Array<number>) => {
            // Assert
            ticketListRow.forEach((lotteryNumber:number, index, lotteryTicket) => {
                lotteryTicket.splice(index, 1);
                expect(lotteryTicket).not.to.include(lotteryNumber);
            })
        });
    });

});