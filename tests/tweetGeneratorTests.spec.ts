import assert from "assert";
import { expect } from "chai";
import TweetGenerator from '../tweetGenerator';
import { tweetStatus } from "../Interfaces";

describe("Tweet generator", () => {
    const wrongWeeknumberList = [-2, -1, 0, 53, null, 69, NaN];
    const wrongAwardAmountList = [0, null, NaN];
    const statusList: Array<tweetStatus> = [
        { status: "begin" },
        { status: "numbersGenerated", additionalString: "11,22,5,6,7,20,6,17,3,8,40,39,21,18,29,4" },
        { status: "waitingForResults" },
        { status: "resultsReceived", additionalString: "123", results: 1 },
        { status: "resultsReceived", additionalString: "456", results: 2 },
        { status: "resultsReceived", additionalString: "789", results: 3 },
        { status: "resultsReceived", additionalString: "420", results: 4 },
        { status: "resultsReceived", additionalString: "69", results: 5 },
        { status: "resultsReceived", additionalString: "1337", results: 6 },
        { status: "resultsReceived", additionalString: "123456789", results: 7 },
        { status: "bingo", additionalString: null },
        { status: "Saab 900", additionalString: "bar", results: 123456 }
    ];
    
    // beforeEach(() => {
    //     console.log("executes before every test");
    // });

    it("should generate a tweet generator", () => {
        // Arrange
        const weekNumber = 1;

        // Act
        const tweetGenerator = new TweetGenerator(weekNumber);

        // Assert
        expect(tweetGenerator).to.have.property("results");
        expect(tweetGenerator).to.have.property("tweetTemplates");
    });

    wrongWeeknumberList.forEach((wrongWeekNumber: number|null) => {
        const weekNumberString = (!Number.isNaN(wrongWeekNumber) && wrongWeekNumber !== null) ? wrongWeekNumber.toString() : typeof wrongWeekNumber;
        it(`should not generate a tweet generator if given wrong weeknumber ${weekNumberString}`, () => {
            // Assert only, no need to arrange or act.
            assert.throws(
                () => new TweetGenerator(wrongWeekNumber),
                Error,
                "Viikkonumero väärä"
            );
        });
    });

    it("should return 'float looking string' as amount of award money if given valid number", () => {
        // Arrange
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const awardFloat = Math.floor(Math.random() * (1000000 - 1) + 1);
        const tweetGenerator = new TweetGenerator(weekNumber);

        // Act
        const awardMoneyString = tweetGenerator.awardToString(awardFloat);
        // Assert
        expect(awardMoneyString).to.equal((awardFloat / 100).toFixed(2).replace(".", ","));
    });

    wrongAwardAmountList.forEach((wrongAmountAmount: any) => {
        const awardAmountString = typeof wrongAmountAmount;
        it(`should not generate a tweet generator if given wrong weeknumber ${awardAmountString}`, () => {
            // Arrange
            const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
            const tweetGenerator = new TweetGenerator(weekNumber);

            // Act
            const awardMoneyString = tweetGenerator.awardToString(wrongAmountAmount);
            // Assert
            expect(awardMoneyString).to.equal("ei mitään");
        });
    });

    it(`should return proper tweet when given begin as status`, () => {
        // Arrange
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const statusThing = statusList[0];
        const tweetGenerator = new TweetGenerator(weekNumber);
        const statusString = statusThing.additionalString?.toString();
        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status, statusString, statusThing.results);

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
    });

    it(`should return proper tweet when given afterGenerating as status`, () => {
        // Arrange
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const statusThing = statusList[1];
        // TODO: Maybe randomize this value?
        statusThing.additionalString = "1,2,3,4,5,6,7";
        const tweetGenerator = new TweetGenerator(weekNumber);

        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status, statusThing.additionalString);

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
        expect(statusTweetText).to.equal(`No, pistin 20 riviä tulemaan, valitsin numerot ${statusThing.additionalString}`);
    });

    it(`should return proper tweet when given waitingForResults as status`, () => {
        // Arrange
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const statusThing = statusList[2];
        const tweetGenerator = new TweetGenerator(weekNumber);

        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status)

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
        expect(statusTweetText).to.equal("Tuloksia oottelen.");
    });

    it(`should return proper tweet when given loser resultsReceived as status`, () => {
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const statusThing = statusList[3];
        const statusString = statusThing.additionalString?.toString();
        // Arrange
        const tweetGenerator = new TweetGenerator(weekNumber);
        statusThing.results = 0;

        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status, statusString, statusThing.results);

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
        expect(statusTweetText).to.contain(tweetGenerator.tweetTemplates.losingResultsReceived);
    });

    it(`should return proper tweet when given winning resultsReceived as status`, () => {
        // Arrange
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const statusThing = statusList[3];
        const tweetGenerator = new TweetGenerator(weekNumber);
        const winnerTweetTemplateList = tweetGenerator.tweetTemplates.winningResultsReceived.split(/{\w+}/);
        statusThing.results = Math.floor(Math.random() * (6 - 1) + 1);
        const award = (statusThing.results > 3) ? "numeroLuku" : "nolla"
        statusThing.additionalString = award;

        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status, statusThing.additionalString, statusThing.results);

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
        // "I think regex is useful here" said the developer not knowing if other type would be useful. - Kare, 2022-01-23
        expect(statusTweetText).not.to.match(/({|})/, "Templaten replace ei toimi.");
        // TODO Test that things in winnerTweetTemplateList can be found in statusTweetText
        // expect(statusTweetText).to.have.all.members(winnerTweetTemplateList);
    });

    it(`should return proper tweet when given jackpot as status`, () => {
        // Arrange
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const tweetGenerator = new TweetGenerator(weekNumber);
        const statusThing = statusList[3];
        const statusString = statusThing.additionalString?.toString();
        statusThing.results = 7;

        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status, statusString, statusThing.results);

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
        expect(statusTweetText).to.equal(tweetGenerator.tweetTemplates.jackpot);
    });

    it("should return proper tweet when given random stuff as status", () => {
        // Arrange
        
        const weekNumber = Math.floor(Math.random() * (52 - 1) + 1);
        const tweetGenerator = new TweetGenerator(weekNumber);
        const randomStatusIndex = (Math.random() <= 0.5) ?  statusList.length-1 : statusList.length-2; 
        const statusThing = statusList[randomStatusIndex];
        const statusString = statusThing.additionalString?.toString();
        // Act
        const statusTweetText = tweetGenerator.getTweetText(statusThing.status, statusString, statusThing.results);

        // Assert
        expect(statusTweetText).to.be.a('string');
        expect(statusTweetText).not.to.be.null;
        expect(statusTweetText).not.to.be.empty;
        expect(statusTweetText).to.equal("En tiedä mitä vastata.");
    });

    statusList.forEach(statusObject => {
        const statusName = statusObject.status;
        let tweetTextTemplate: RegExp;
        
        // Is it switch-case-time? I think it's switch-case time.
		// Switch-case-time!
		switch(statusName){
			case "begin":
                // Regexp solo!
				tweetTextTemplate = new RegExp(/\w{5,}/);
				break;
			case "numbersGenerated":                
                tweetTextTemplate = new RegExp(/(No\, pistin 20 riviä tulemaan\, valitsin numerot )(\d{1,2}\,){6}\d{1,2}/);
				break;
			case "waitingForResults":
				tweetTextTemplate = new RegExp("(Tuloksia oottelen.)");
				break;
			default:
				tweetTextTemplate =  new RegExp("En tiedä mitä vastata.");
		}
        it(`should return valid tweet text based on status '${statusName}'`, () => {
            // Arrange
            const weekNumber = 1;
            const tweetGenerator = new TweetGenerator(weekNumber);
            const statusString = statusObject.additionalString?.toString();

            if (statusName == "resultsReceived") {
                if (statusObject.results && statusObject.results <= 3) {
                    tweetTextTemplate = new RegExp(/Lottonumerot arvottu ja sain mä (yhden|kaksi|kolme) oikein\. Eli voitin nolla euroa\./);
                } else if (statusObject.results && statusObject.results > 3 && statusObject.results < 7) {
                    // Using the \D here because characters with umlaut aren't matched in \w
                    // Also I don't feel like googling how \w can be set to match öäå characters
                    tweetTextTemplate = new RegExp(/Lottonumerot arvottu ja (sain mä|meni jopa) \D{5} oikein\. Eli voitin \d+ euroa\./);
                } else {
                    tweetTextTemplate = new RegExp("(Jos oisin oikeesti lotonnu niin oisin tienannu {award} mutta koska olen vain botti niin kehittäjääni ei nyt vituta.)");
                }
            }
            // Act
            const statusTweetText = tweetGenerator.getTweetText(statusName, statusString, statusObject?.results);
    
            // Assert
            expect(statusTweetText).to.match(tweetTextTemplate, 
                `Wrong tweet returned when status: '${statusName}', additionalString: '${statusObject.additionalString}', results: ${statusObject.results}`
            );
        });
    });

});