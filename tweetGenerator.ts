export default class TweetGenerator {
	public tweetTemplates: {
		begin: string,
		afterGenerating: string,
		winningResultsReceived: string,
		losingResultsReceived: string,
		jackpot: string
	};
	private results: {
		1: string,
		2: string,
		3: string,
		4: string,
		5: string,
		6: string,
		7: string
	}
	constructor(weekNumber: number|null) {
		if (isNaN(weekNumber) || weekNumber <= 0 || weekNumber > 52) {
			throw new Error("Viikkonumero väärä");
		}
		this.tweetTemplates = {
			begin: this.startTweetGenerator(weekNumber),
			afterGenerating: "No, pistin 20 riviä tulemaan, valitsin numerot {numbers}",
			winningResultsReceived: "Lottonumerot arvottu ja {myResults} Eli voitin {award} euroa.",
			losingResultsReceived: "Lottonumerot arvottu eikä yhtään oikein.",
			jackpot: "Jos oisin oikeesti lotonnu niin oisin tienannu {award} mutta koska olen vain botti niin kehittäjääni ei nyt vituta."
		}
		this.results = {
			1: "sain mä yhden oikein.",
			2: "sain mä kaksi oikein.",
			3: "sain mä kolme oikein.",
			4: "sain mä neljä oikein.",
			5: "sain mä viisi oikein.",
			6: "meni jopa kuusi oikein.",
			7: "kaikki oikein!"
		}
	}
	
	private startTweetGenerator(givenIndex: number):string {

		// This ain't no fancy big city AI code so I'll just list a bunch of starters and pick one of them
		// to fix "Twitter won't let post same thing twice"-bug
		
		const listOfStartersForLotota:Array<string> = [
			"Pitäsköhän", "Taas tarttis", "Ois taas aika", "Siitähän on jo viikko kulunu niin taas on aika",
			"Siitähän on jo aikaa, joten taas on pitää", "Kaikesta huolimatta on aika", "Niinkuin viime viikolla, on tälläkin viikolla tarkoitus",
			"Aina mun pitää", "Taas mun pitää", "Kun ei muu päätäni pakota, tulee minun",  "Äh taas pitää", 
			"Mä botti oon ja tahdon", "Tarttis ny", "Laskujeni mukaan on aika", "Se ois aika", "Jonkun skriptin mukaan olisi aika",
			"Pitää taasen", "On aika", "Kyää ny pitää", "Ei kovin paljoa haluta mut pakko mun on", "Tarpeellista toiminnalleni on",
			"Karmastani huolimatta aion", "Koska en rahaa tähän oikeasti käytä niin aion", "Mulla on nyt aika", "Seuraavaksi tämä botti aikois" 
		];
		
		const listOfStartersForLottois:Array<string> = [
			"Jospa sitä", "Siitähän on jo viikko kulunu niin jospa", "Vaikken siitä nettois niin kai mun pitää", 
			"Mitäpä tässä muuta vois ku", "Nyt tämä botti aikois tehä niin et", "Oukei bois nau is taim for"
		];
		const generalExpressions:Array<string> = [
			"Ka jos mää sit vaa lottoisin", "Lottoriviä tulille", "GENERATE LOTTORIVIT", "Minäpä tässä luon lottorivejä",
			"Teen kai lottorivini väärin", "Tässä kohtaa alkaa näiden twiittien keksiminen olla hankalaa", 
			"Jos luet tämän, olen varmaan generoinut ne lottorivit", "Bönthöö, mättöö ja lottooo!", 
			"Kohta pitää luoda tämän viikon viralliset lottorivini", "Oikein hyvää. Arvon lottorivini.", 
			"Arvon lukijoille tiedoksi että arvon lottorivini", "Näiden ilmoitustwiittien luominen alkaa väsyttää",
			"Seuraa lottorivieni arvonta, saas nähä onko veikkaus arponu samoja numeroita", "Ilmoitus syklin alkamisesta",
			"Lottoamisesta välittämättä trans oikeudet ovat ihmisoikeudet", "Lottorivien numeroita ennen tuleva ilmoitus twiitti",
			"Piip Puup satunnaislukugeneraattorini käyntiin", "Seuraavaksi taas lottorivejä"
		];
		const useLotota = givenIndex < listOfStartersForLotota.length;
		const useLottois = givenIndex > listOfStartersForLotota.length;
		const useGeneric = givenIndex > (listOfStartersForLotota.length+listOfStartersForLottois.length);

		if (useLotota && !useLottois) {
			return listOfStartersForLotota[givenIndex] + " lotota";
		} else if (useLottois && !useGeneric) {
			givenIndex = (givenIndex === listOfStartersForLotota.length) ? 0 : givenIndex-listOfStartersForLotota.length;
			return listOfStartersForLottois[givenIndex] + " lottois";
		} else {
			const combinedArraysLength = listOfStartersForLotota.length+listOfStartersForLottois.length;
			givenIndex = (givenIndex === combinedArraysLength) ? 0 : givenIndex-combinedArraysLength;
			return generalExpressions[givenIndex];
		}
	}
	private noWinningTweetGenerator(givenIndex: number):string {
		const listOfCurses: Array<string> = [
			"Perhana", "Saamari", "Turkanen", "Hemmetti", "Jumankavita",
			"Sviddunääs", "DISAPPOINTED", ":(", ";_;"
		];
		const listOfLoserTweets:Array<string> = [
			"Lottonumerot arvottu eikä yhtään oikein.",
			"Lottonumerot arvottu eikä yhtään oikein taaskaan.",
			"Ei yhtään oikein vieläkään",
			"Ei voittoo, ei palkintoo",
			"Tykhe ei minua siunannut",
			"Numerot arvottu muttei ollut mun numeroita mukana",
			"Ei taaskaan mitään tullu voi helevetti",
			"En voittanut mitään",
			"Hävisin taas",
			"Lakshmi on mut hylännyt",
			"Hävisin kokonaan",
			"Ei voiton voittoa",
			"Veikkaus vei rahani",
			"Sinne meni nekin mielikuvituseurot",
			"Arvaa kuka voitti? NO EN MINÄ",
			"Onni ei minua suosinut",
			"Ei voittoa.",
			"Onni ei minua suosinut tälläkään kertaa",
			"Ei yhtään numeroa oikein",
			"Vain vääriä numeroita valitsin",
			"Fortuna vaan kirosi minut"
		];
		const tweetListLength = listOfLoserTweets.length;
		const useGeneric = givenIndex < tweetListLength;
		if (useGeneric) {
			return listOfLoserTweets[givenIndex];
		} else {
			let generatedTweetIndex: number = givenIndex % tweetListLength;			
			let generatedCurseIndex: number = (generatedTweetIndex > 0) 
				? Math.trunc(givenIndex / tweetListLength)
				: givenIndex / tweetListLength;
			
			const curse: string = listOfCurses[generatedCurseIndex-1];
			const tweet: string = listOfLoserTweets[generatedTweetIndex];
			
			return `${curse} ${tweet}`; 
		}
	}
	public awardToString(award: number):string {
		return (award === null || award === 0 || isNaN(award)) ? "ei mitään" : (award / 100).toFixed(2).replace(".", ",");
	}
	// TODO: Change the parameters to a single Interface to help function calling.
	public getTweetText(status: string, additionalString?:string, results?:number):string {
		let tweet: string;
		// Switch-case-time!
		switch(status){
			case "begin":
				tweet = this.tweetTemplates.begin;
				break;
			case "numbersGenerated":
				tweet = this.tweetTemplates.afterGenerating.replace("{numbers}", additionalString);
				break;
			case "waitingForResults":
				tweet = "Tuloksia oottelen.";
				break;
			case "resultsReceived":
				if (results < 1 ) {
					tweet = this.tweetTemplates.losingResultsReceived;
				} else {
					if (results !== 7) {
						let myResults = this.results[results];
						additionalString = (results > 3) ? additionalString : "nolla";
						tweet = this.tweetTemplates.winningResultsReceived
							.replace("{myResults}", myResults)
							.replace("{award}", additionalString);
					} else {
						tweet = this.tweetTemplates.jackpot;
					}
					
				}
				break;
			default:
				tweet =  "En tiedä mitä vastata.";
		}
		return tweet;
	}
}