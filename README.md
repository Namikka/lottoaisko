# Lottoaisko
---
## An app that asks if it should do lottery with a list of generated numbers?
It generates a lottery ticket with list of numbers that cost 20€. Then it tweets/posts them to social media few minutes before the winning numbers are published and then reports if it won anything.
Also at somepoint it posts a month/year report on how much money it has spent, how much it has won, what probability it has to win the jackpot and maybe something else.

## Status
`Development`

## TODO:
- Make tests
- See if the whole lottoStats.json thing can be used in Azure
    - Nope, unless I dig up money for storage account and currently my budget is 0€
- If not then where do we put the tickets?
    - We deploy this to linux server where we store them to JSON-file
    - This requires environment variables added.
- Create functionalilty that cycles through the list of intro tweets
- deploy it