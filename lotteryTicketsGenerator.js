"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
const https = __importStar(require("https"));
const zlib = __importStar(require("zlib"));
class lottoNumerot {
    constructor() {
        this.tickets = 20;
        this.lotteryRowMaxLength = 7;
        this.maxLotteryNumber = 40;
        this.minLotteryNumber = 1;
        this.veikkausAPIAddress = "https://www.veikkaus.fi/api/draw-results/v1/games/LOTTO/draws/by-week/";
        let lotteryNumber;
        let newTicketList = new Array();
        this.awardsObject = { four: 0, five: 0, six: 0, seven: 0 };
        do {
            let lotteryRow = new Array();
            do {
                lotteryNumber = this.numberGenerator();
                if (lotteryRow.includes(lotteryNumber) === false || lotteryRow.length == 0)
                    lotteryRow.push(lotteryNumber);
            } while (lotteryRow.length < this.lotteryRowMaxLength);
            if (newTicketList.includes(lotteryRow) === false || newTicketList.length == 0) {
                lotteryRow.sort(function (a, b) { return a - b; });
                newTicketList.push(lotteryRow);
            }
            if (newTicketList.length == this.tickets) {
                this.ticketList = newTicketList;
            }
        } while (newTicketList.length !== this.tickets);
    }
    // Since this is going to be asynchronous functionality, we need a promise.
    // Also I realized that the award sums are received this way so we gotta set those values too.
    getWinningNumbers() {
        const kelloNyt = new Date();
        return new Promise((resolve, reject) => {
            const weekNumber = this.getWeek(kelloNyt) - 1;
            this.veikkausAPIAddress += kelloNyt.getFullYear().toString() + "-W" + weekNumber.toString();
            let winningNumbers;
            let awards = { four: 0, five: 0, six: 0, seven: 0 };
            const unzipper = zlib.createGunzip();
            https.get(this.veikkausAPIAddress, (res) => {
                let bodyString = "";
                // Since we get either a gzipped response or just JSON
                if (res.headers["content-encoding"] === "gzip") {
                    res.pipe(unzipper);
                    unzipper.on('data', function (data) {
                        bodyString += data;
                    });
                    unzipper.on('end', function () {
                        let returnableObject = JSON.parse(bodyString);
                        // TODO: Clean or flat the content.
                        // Set the results, the received array is a bit weird so we have to handle that.
                        awards.seven = (returnableObject[0].prizeTiers[0].shareAmount !== 0) ? returnableObject[0].prizeTiers[0].shareAmount : 1000000;
                        awards.six = returnableObject[0].prizeTiers[2].shareAmount;
                        awards.five = returnableObject[0].prizeTiers[3].shareAmount;
                        awards.four = returnableObject[0].prizeTiers[4].shareAmount;
                        winningNumbers = returnableObject[0].results[0].primary;
                        resolve(winningNumbers);
                    });
                }
                else {
                    // In case the content encoding is different, we handle it like this
                    res.on("data", (data) => {
                        bodyString += data;
                    });
                    res.on("end", () => {
                        try {
                            fs.writeFileSync("logdump.txt", bodyString);
                            let returnableObject = JSON.parse(bodyString);
                            // TODO: Clean or flat the content.
                            // Set the results, the received array is a bit weird so we have to handle that.
                            awards.seven = (returnableObject[0].prizeTiers[0].shareAmount !== 0) ? returnableObject[0].prizeTiers[0].shareAmount : 1000000;
                            awards.six = returnableObject[0].prizeTiers[2].shareAmount;
                            awards.five = returnableObject[0].prizeTiers[3].shareAmount;
                            awards.four = returnableObject[0].prizeTiers[4].shareAmount;
                            winningNumbers = returnableObject[0].results[0].primary;
                            resolve(winningNumbers);
                        }
                        catch (error) {
                            console.error("Parsing the non-gzipped data failed: ", error.message);
                            reject(error);
                        }
                        ;
                    });
                }
                // This needs to be here because the value setting works here. 
                this.awardsObject = awards;
            }).on('error', (e) => {
                console.error("veikkausAPIAddress get failed: ", e);
                reject(e);
            });
        });
    }
    ;
    getWeek(givenDate) {
        let date = new Date(givenDate.getTime());
        date.setHours(0, 0, 0, 0);
        // Thursday in current week decides the year.
        date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
        // January 4 is always in week 1.
        let week1 = new Date(date.getFullYear(), 0, 4);
        // Adjust to Thursday in week 1 and count number of weeks from date to week1.
        return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
    }
    ;
    numberGenerator() {
        const min = Math.ceil(this.minLotteryNumber);
        const max = Math.floor(this.maxLotteryNumber);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
exports.default = lottoNumerot;
//# sourceMappingURL=lotteryTicketsGenerator.js.map