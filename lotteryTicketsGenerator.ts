import * as fs from 'fs';
import * as https from 'https';
import * as zlib from "zlib";
import { IncomingMessage } from 'http';

export default class lottoNumerot {
	public ticketList: Array<Array<number>>;
	public winningNumbers: Array<number>;
	public awardsObject: {
		four: number, 
		five: number,
		six: number,
		seven: number
	};
	readonly tickets: number = 20;
	readonly lotteryRowMaxLength: number = 7;
	readonly maxLotteryNumber: number = 40;
	readonly minLotteryNumber: number = 1;
	veikkausAPIAddress:string = "https://www.veikkaus.fi/api/draw-results/v1/games/LOTTO/draws/by-week/";
	
	constructor() {
		let lotteryNumber: number;
		let newTicketList: Array<Array<number>> = new Array();
		this.awardsObject = { four: 0, five: 0,	six: 0,	seven: 0 };
		do {
			let lotteryRow: Array<number> = new Array();
			do {
				lotteryNumber = this.numberGenerator();
				if (lotteryRow.includes(lotteryNumber) === false || lotteryRow.length == 0)
					lotteryRow.push(lotteryNumber);
			} while (lotteryRow.length < this.lotteryRowMaxLength);
			
			if (newTicketList.includes(lotteryRow) === false || newTicketList.length == 0) {
				lotteryRow.sort(function(a, b){return a-b});
				newTicketList.push(lotteryRow);
			}
			if (newTicketList.length == this.tickets) {
				this.ticketList = newTicketList;
			}
		} while (newTicketList.length !== this.tickets);
	}
	
	// Since this is going to be asynchronous functionality, we need a promise.
	// Also I realized that the award sums are received this way so we gotta set those values too.
	public getWinningNumbers():Promise<Array<string>> {
		const kelloNyt = new Date();
		return new Promise((resolve, reject) => {
			const weekNumber = this.getWeek(kelloNyt)-1; 
			this.veikkausAPIAddress += kelloNyt.getFullYear().toString() + "-W" + weekNumber.toString();
			let winningNumbers: Array<string>;
			let awards =  { four: 0, five: 0, six: 0, seven: 0 };
			const unzipper = zlib.createGunzip();

			https.get(this.veikkausAPIAddress, (res: IncomingMessage) => {
				let bodyString = "";
				// Since we get either a gzipped response or just JSON
				if (res.headers["content-encoding"] === "gzip") {
					res.pipe(unzipper);
				
					unzipper.on('data', function (data) {
						bodyString += data;
					});
				
					unzipper.on('end', function() {
						let returnableObject = JSON.parse(bodyString);
						// TODO: Clean or flat the content.
						// Set the results, the received array is a bit weird so we have to handle that.
						awards.seven = (returnableObject[0].prizeTiers[0].shareAmount !== 0) ? returnableObject[0].prizeTiers[0].shareAmount : 1000000;
						awards.six = returnableObject[0].prizeTiers[2].shareAmount;
						awards.five = returnableObject[0].prizeTiers[3].shareAmount;
						awards.four = returnableObject[0].prizeTiers[4].shareAmount;
						winningNumbers = returnableObject[0].results[0].primary;						
						resolve(winningNumbers);
					});
				} else {
					// In case the content encoding is different, we handle it like this
					res.on("data", (data) => {
						bodyString += data;
					});
					res.on("end", () => {
						try {
							fs.writeFileSync("logdump.txt", bodyString);
							let returnableObject = JSON.parse(bodyString);
							// TODO: Clean or flat the content.
							// Set the results, the received array is a bit weird so we have to handle that.
							awards.seven = (returnableObject[0].prizeTiers[0].shareAmount !== 0) ? returnableObject[0].prizeTiers[0].shareAmount : 1000000;
							awards.six = returnableObject[0].prizeTiers[2].shareAmount;
							awards.five = returnableObject[0].prizeTiers[3].shareAmount;
							awards.four = returnableObject[0].prizeTiers[4].shareAmount;
							winningNumbers = returnableObject[0].results[0].primary;						
							resolve(winningNumbers);
						} catch (error) {
							console.error("Parsing the non-gzipped data failed: ", error.message);
							reject(error);
						};
					});
				}				
				// This needs to be here because the value setting works here. 
				this.awardsObject = awards;
			}).on('error', (e: Error) => {
				console.error("veikkausAPIAddress get failed: ", e);
				reject(e);
			});
        });		
	};

	public getWeek(givenDate: Date) {
		let date = new Date(givenDate.getTime());
		date.setHours(0, 0, 0, 0);
		// Thursday in current week decides the year.
		date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
		// January 4 is always in week 1.
		let week1 = new Date(date.getFullYear(), 0, 4);
		// Adjust to Thursday in week 1 and count number of weeks from date to week1.
		return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
	};

	private numberGenerator(): number {
		const min = Math.ceil(this.minLotteryNumber);
		const max = Math.floor(this.maxLotteryNumber);
		return Math.floor(Math.random() * (max - min + 1)) + min; 
	}
	
}